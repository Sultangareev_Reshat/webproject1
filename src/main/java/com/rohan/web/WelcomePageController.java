package com.rohan.web;

import java.util.ArrayList;
import java.util.List;

import org.apache.catalina.core.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class WelcomePageController {
	@Autowired
	private org.springframework.context.ApplicationContext context;
	@Autowired
	JpaRepoForPhrases jpfp;
	@Autowired
	JpaRepoForUsers jpfu;
	@Autowired
	PhraseStorage ps;
	@Autowired
	CUser user;
	

	
	@RequestMapping("/")
	private ModelAndView welcome()
	{
		ModelAndView mv = new ModelAndView();
		mv.setViewName("WelcomePage");
		return mv;
	}
	
	@RequestMapping("enterName")
	private ModelAndView enterName(@RequestParam("input")String name)
	{
		List<CUser> userList;
		if(jpfu.count()>0)
		{
		userList = jpfu.findByname(name);
		user.setAccessType(userList.get(0).getAccessType());
		user.setName(userList.get(0).getName());

		System.out.println(user.getAccessType());
		}
		else
		{
		CUser nUser = context.getBean(CUser.class);
		nUser.setName(name);
		nUser.setAccessType("partial");
		user = nUser;
		jpfu.save(nUser);
		}
		ModelAndView mv = new ModelAndView();
		mv.setViewName("MainPage");
		mv.addObject("name", name);
//		List<PhraseStorage> listPhrase = jpfp.findAll();
//		
//		mv.addObject("pageText",listPhrase);
		return mv;
	}
	
	
}
