package com.rohan.web;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface JpaRepoForUsers extends JpaRepository<CUser, Integer> {
	
	@Query("from CUser where name=:name")
	List<CUser> findByname(String name);

}
