package com.rohan.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ChatController {
	@Autowired
	private org.springframework.context.ApplicationContext context;
	@Autowired
	JpaRepoForPhrases jpfp;
	@Autowired
	PhraseStorage ps;
	@Autowired
	CUser user;
	ModelAndView mv = new ModelAndView();

	@RequestMapping("/chat")
	private ModelAndView chat(@RequestParam("inputForChat")String input)
	{
		mv.setViewName("ChatPage");
		input = input.trim();
		System.out.println(user.getName() + user.getAccessType());
		if(input.contains("add") && user.getAccessType().contains("full"))
		{
			System.out.println("new phrase");
			return newPhraseAdd(input, mv);
		}
		else if(input.contains("add"))
		{
			System.out.println("cont add");
			mv.addObject("pageText", "You do not have permission, try use user Reshat");
			return mv;
		}
		else
		{
			System.out.println("else");
			return findPhrase(input,mv);
		}
		
	}
	public ModelAndView findPhrase(String input, ModelAndView mv)
	{
		List<PhraseStorage> output = jpfp.findByphrase(input);
		System.out.println("intput" + input);
		System.out.println(output);
		if(output.size() > 1)
		{
			System.out.println("Error found too many records in /chat");
			if( (input == "hello") || (input == "Hello"))
			mv.addObject("pageText", output.get(0).getPhraseOut() + " "+ user.getName());
			else 
				mv.addObject("pageText", output.get(0).getPhraseOut());
		}
		else if(output.size()  == 1)
		{
			if((input == "hello") || (input == "Hello"))
		mv.addObject("pageText", output.get(0).getPhraseOut() +" "+ user.getName());
			else 
				mv.addObject("pageText", output.get(0).getPhraseOut());
		}
		else
		{
			System.out.println("Error, not nothing found in /chat");
			mv.addObject("pageText", "You can add phrase using: add YOUR PHRASE to OUTPUTphrase");
		}
		return mv;
	}
	public ModelAndView newPhraseAdd(String input, ModelAndView mv)
	{
		String InputCut = "add ";
		String OutputCut = "to ";
		System.out.println(InputCut.length());
		int index = input.indexOf(OutputCut);
		String str = input.substring(index + OutputCut.length(),input.length()).trim();
		String str1 = input.substring(InputCut.length(),index).trim();
		ps.setPhraseIn(str1);
		ps.setPhraseOut(str);
		jpfp.save(ps);
		mv.addObject("pageText", "Phrase successfully added");
		return mv;
	}
}
