package com.rohan.web;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Entity
public class PhraseStorage {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int Id;
	private String phrase;
	private String PhraseOut;
	
	public String getPhraseIn() {
		return phrase;
	}
	public void setPhraseIn(String phrase) {
		this.phrase = phrase;
	}
	public String getPhraseOut() {
		return PhraseOut;
	}
	public void setPhraseOut(String phraseOut) {
		PhraseOut = phraseOut;
	}
	@Override
	public String toString() {
		return "PhraseStorage [Id=" + Id + ", phrase=" + phrase + ", PhraseOut=" + PhraseOut + "]";
	}
	

}
