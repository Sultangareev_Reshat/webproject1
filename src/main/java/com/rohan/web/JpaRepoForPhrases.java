package com.rohan.web;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface JpaRepoForPhrases extends JpaRepository<PhraseStorage, Integer>{
//	@Query("from PhraseStorage where phrase=:phr and username =:username")
//	List<PhraseStorage> findByphraseandusername(String phr, String username);
	@Query("from PhraseStorage where phrase=:phr")
	List<PhraseStorage> findByphrase(String phr);
		
}
//List<Alien> findByTech(String tech);


